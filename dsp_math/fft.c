#include "main.h"
#include "fft.h"
#include "arm_math.h"
#include "arm_const_structs.h"

#define THREAD_PRIORITY 20
#define THREAD_STACK_SIZE 7168
#define THREAD_TIMESLICE 20

#define FFT_LENGTH        1024         //FFT长度，默认是1024点FFT

extern __IO uint16_t ADC_ConvertedValue[1024];

float fft_inputbuf[FFT_LENGTH*2];    //FFT输入数组
float fft_outputbuf[FFT_LENGTH];    //FFT输出数组

//外部事件控制块
extern rt_event_t event;


void fft_init(void)
{	
	//为了代码的完整性
	int i;
	for(i=0;i<FFT_LENGTH;i++)
	{
    	fft_inputbuf[2*i]=(float)ADC_ConvertedValue[i];//实部为ADC采样值
	    fft_inputbuf[2*i+1] = 0;//虚部为0
	}	
}

void fft_entry(void *param)
{
	 rt_uint32_t e;
	 int i;
		while(1)
		{
      //接收到DMA传输中断后进行FFT变换操作
      if(rt_event_recv(event,(1<<3),RT_EVENT_FLAG_OR|RT_EVENT_FLAG_CLEAR,RT_WAITING_FOREVER,&e)==RT_EOK)
			{
		  fft_init();
		
			for(i=0;i<FFT_LENGTH;i++)
			{
				fft_inputbuf[2*i]=(float)ADC_ConvertedValue[i];//实部为ADC采样值
				fft_inputbuf[2*i+1] = 0;//虚部为0
			}	
      arm_cfft_f32(&arm_cfft_sR_f32_len1024,(float*)fft_inputbuf,0,1);
      arm_cmplx_mag_f32((float*)fft_inputbuf,fft_outputbuf,FFT_LENGTH);    //把运算结果复数求模得幅值 
      }
			//rt_kprintf("fft result:%f\n",fft_outputbuf[0]);
			rt_thread_mdelay(300);
		}	
   
	
}
/*****************************用户应用程序***************************/
int fft_sample(void)
{

	
	//指向指针控制块的指针
    rt_thread_t thread;
	//创建名为thread2的线程									
	thread=rt_thread_create("thread_fft",
	                        fft_entry,
	                        RT_NULL,
							            THREAD_STACK_SIZE,
							            THREAD_PRIORITY,
							            THREAD_TIMESLICE											 
	                       );
	if(thread!=RT_NULL)
	rt_thread_startup(thread);//将线程的状态更改为就绪状态，并放到相应优先级队列中等待调度
  
  return 0;	

}

















void fft(void)
{
	
		
		
		//    while(1)
//    {
//        u32 keyval = (u32)keys_scan(0);
//        if(keyval==1)
//        {
//                for(int i=0;i<FFT_LENGTH;i++)//生成信号序列
//                {
//                     fft_inputbuf[2*i]=10+4.5*arm_sin_f32(2*PI*i*200/FFT_LENGTH)+\
//                                          7.5*arm_sin_f32(2*PI*i*350/FFT_LENGTH);

//                     fft_inputbuf[2*i+1]=0;//虚部全部为0
//                }
//               //arm_cfft_sR_f32_len1024，该变量即为"arm_const_structs.h"提供的配置变量，包含头文件后，直接调用即可。
//               arm_cfft_f32(&arm_cfft_sR_f32_len1024,fft_inputbuf,0,1);
//               arm_cmplx_mag_f32(fft_inputbuf,fft_outputbuf,FFT_LENGTH);    //把运算结果复数求模得幅值

//                printf("FFT Result:\r\n");
//                for(int i=0;i<FFT_LENGTH;i++)
//                {
//                    printf("%f\r\n",fft_outputbuf[i]);
//                }
//        }
//        delay_ms(60);
//    }
}

