
#include "usart6.h"
#include "main.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

UART_HandleTypeDef huart6;



/* USART6 init function */

void MX_USART6_UART_Init(void)
{

  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspInit 0 */

  /* USER CODE END USART6_MspInit 0 */
    /* USART6 clock enable */
    __HAL_RCC_USART6_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART6 GPIO Configuration
    PA11     ------> USART6_TX
    PA12     ------> USART6_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART6 interrupt Init */
   HAL_NVIC_SetPriority(USART6_IRQn, 0, 6);
   HAL_NVIC_EnableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspInit 1 */
   
  /* USER CODE END USART6_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspDeInit 0 */

  /* USER CODE END USART6_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART6_CLK_DISABLE();

    /**USART6 GPIO Configuration
    PA11     ------> USART6_TX
    PA12     ------> USART6_RX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

    /* USART6 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART6_IRQn);
  /* USER CODE BEGIN USART6_MspDeInit 1 */

  /* USER CODE END USART6_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
uint8_t serial_data[16];
extern rt_event_t event;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance==USART6)
  {
    //校验来自ESP8285的数据
    if((0x55 == serial_data[0])&&(serial_data[15]==0xed))
    {
		
			//接收到ESP8285的NPT时间和天气信息数据，发送数据
		rt_event_send(event,(1<<2));
		HAL_GPIO_TogglePin(LED2_GPIO_Port,LED2_Pin);
  
    }
    HAL_UART_Receive_IT(&huart6,(uint8_t *)serial_data,16);
  }
}
/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/



/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */

  /* USER CODE END USART6_IRQn 0 */
	
	
	//HAL_UART_Receive(&huart6, serial_data,16,1000);
  HAL_UART_IRQHandler(&huart6);
  /* USER CODE BEGIN USART6_IRQn 1 */

  /* USER CODE END USART6_IRQn 1 */
}


//void rt_hw_console_output(const char *str)
//{

//    rt_size_t i = 0, size = 0;
//    char a = '\r';

//    __HAL_UNLOCK(&huart6);

//    size = rt_strlen(str);
//    for (i = 0; i < size; i++)
//    {
//        if (*(str + i) == '\n')
//        {
//            HAL_UART_Transmit(&huart6, (uint8_t *)&a, 1, 1);
//        }
//        HAL_UART_Transmit(&huart6, (uint8_t *)(str + i), 1, 1);
//    }
//}


//char rt_hw_console_getchar(void)
//{
//    int ch = -1;

//    if (__HAL_UART_GET_FLAG(&huart6, UART_FLAG_RXNE) != RESET)
//    {
//        ch = huart6.Instance->DR & 0xff;
//    }
//    else
//    {
//        if(__HAL_UART_GET_FLAG(&huart6, UART_FLAG_ORE) != RESET)
//        {
//            __HAL_UART_CLEAR_OREFLAG(&huart6);
//        }
//        rt_thread_mdelay(10);
//    }
//    return ch;
//}
