#ifndef _key_h
#define _key_h


#include "main.h"

void MX_KEY_Init(void);

#define K1_Pin GPIO_PIN_8
#define K1_GPIO_Port GPIOA
#define K2_Pin GPIO_PIN_9
#define K2_GPIO_Port GPIOA
#define K3_Pin GPIO_PIN_10
#define K3_GPIO_Port GPIOA


#define KEY1 HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_8)
#define KEY2 HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_9)
#define KEY3 HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_10)


#define KEY1_Press   1
#define KEY2_Press   2
#define KEY3_Press   3

void MX_KEY_Init(void);
uint8_t key_scan(void);


#endif

