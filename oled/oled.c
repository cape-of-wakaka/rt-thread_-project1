#include "oled.h"
#include "main.h"
#include "bitmap.h"
#include "string.h"
#include "stdio.h"


#define THREAD_PRIORITY 19
#define THREAD_STACK_SIZE 4096
#define THREAD_TIMESLICE 40


/****************************线程入口函数*****************************************/
u8g2_t u8g2;

extern uint8_t serial_data[16];

char *myitoa(int value, char *string, int radix)
{
    int     i, d;
    int     flag = 0;
    char    *ptr = string;

    /* This implementation only works for decimal numbers. */
    if (radix != 10)
    {
        *ptr = 0;
        return string;
    }

    if (!value)
    {
        *ptr++ = 0x30;
        *ptr = 0;
        return string;
    }

    /* if this is a negative value insert the minus sign. */
    if (value < 0)
    {
        *ptr++ = '-';

        /* Make the value positive. */
        value *= -1;
    }

    for (i = 10000; i > 0; i /= 10)
    {
        d = value / i;

        if (d || flag)
        {
            *ptr++ = (char)(d + 0x30);
            value -= (d * i);
            flag = 1;
        }
    }

    /* Null terminate the string. */
    *ptr = 0;

    return string;

} /* NCL_Itoa */

void showday(int years,int months,int days,int weekdays)
{
  u8g2_ClearBuffer(&u8g2);
	u8g2_SetFont(&u8g2,u8g2_font_ncenB12_tr);
}


void oled_init(void)
{	
	 u8g2Init(&u8g2);
   u8g2_SetPowerSave(&u8g2,0); 
	 u8g2_ClearDisplay(&u8g2);
	 u8g2_DrawStr(&u8g2,0,8,"YKK");
}

void show_time()
{
	uint8_t year_H,year_L, months,days,hours, minutes, seconds,weekdays;
  uint16_t year=(uint16_t)serial_data[7]<<8|serial_data[8];
	char currentTime_hm[] = "";
	char currentTime_s[] = "";

	months=serial_data[9];//
  days=serial_data[10];
  weekdays=serial_data[11];//
  hours=serial_data[12];
  minutes=serial_data[13];//
  seconds=serial_data[14];
  
}

void oled_entry(void *param)
{
	while(1)
	{
	//u8g2_DrawCircle(&u8g2,64,16,15,U8G2_DRAW_ALL);
	//u8g2_DrawBox(&u8g2,64,16,16,U8G2_DRAW_ALL);
//	u8g2_DrawXBMP(&u8g2,0,0,128,32,bilibilitv_12832);
//	u8g2_SendBuffer(&u8g2);
//	u8g2_ClearBuffer(&u8g2);
//	rt_thread_mdelay(1000);
//	u8g2_DrawXBMP(&u8g2,1,1,30,30,Sunny);
//	u8g2_SetFont(&u8g2,u8g2_font_luBIS08_tr);
//	u8g2_DrawStr(&u8g2,33,30,"Sunny");
//	u8g2_SendBuffer(&u8g2);
//	u8g2_ClearBuffer(&u8g2);
//	rt_thread_mdelay(1000);
	u8g2_SetFont(&u8g2,u8g2_font_t0_22b_me);
	u8g2_DrawStr(&u8g2,16,32,"RT-Thread");
	u8g2_SendBuffer(&u8g2);
	u8g2_ClearBuffer(&u8g2);
	rt_thread_mdelay(1000);
	
	}
}
/*****************************用户应用程序***************************/
int oled_sample(void)
{

	oled_init();
	//指向指针控制块的指针
    rt_thread_t thread;
	
	//创建名为thread2的线程									
	thread=rt_thread_create("thread_oled",
	                        oled_entry,
	                        RT_NULL,
							            THREAD_STACK_SIZE,
							            THREAD_PRIORITY,
							            THREAD_TIMESLICE											 
	                       );
	if(thread!=RT_NULL)
	{
		rt_thread_startup(thread);//将线程的状态更改为就绪状态，并放到相应优先级队列中等待调度
	  rt_kprintf("oled_thread success\n");
	}
  else
		rt_kprintf("oled_thread fail\n");
	
	
  return 0;	

}

MSH_CMD_EXPORT(oled_sample,oled sample);


/********************************************************************************************************************/
/*
线程睡眠：需要让运行的当前线程延迟一段时间，在指定的
时间到达后重新运行，叫做“线程睡眠”
线程睡眠使用以下三个函数：

rt_err_t rt_thread_sleep(rt_tick_t tick);
rt_err_t rt_thread_delay(rt_tick_t tick);
rt_err_t rt_thread_mdelay(rt_int32_t ms);

这三个函数接口的作用相同，调用它们可以使当前线程挂起一段指定的时间，
当这个时间过后，线程会被唤醒并再次进入就绪状态

*******************************************************************/


/********************************************************
rt_thread_create()创建动态线程
rt_thread_init()初始化一个静态线程

1.动态线程是系统自动从动态内存堆上分配栈空间与线程句柄
（初始化heap之后才能使用create创建动态线程）；静态线程
是由用户分配栈空间与线程句柄（内核创建线程）
分配的栈空间是按照rtconfig.h中配置的RT_ALIGN_SIZE方式对齐
2.脱离线程列表方式
  rt_thread_create() ---> rt_thread_delete()
  rt_thread_init() ----> rt_thread_detach()
********************************************************/


/*******************************************************
RO Size Flash
code:代码段，存放程序的代码部分
RO-data:只读数据段，存放程序中定义的常量const

RW Size RAM
RW-data:读写数据段，存放初始化为非0的全局变量
ZI-data:0数据段，存放未初始化的全局变量及初始化为0的变量

*********************************************************/














































