#ifndef _bitmap_h
#define _bitmap_h


//---------------------------位图信息--------------------------------
//24*24小电视
const uint8_t bilibilitv_24u[] U8X8_PROGMEM = {
0x00, 0x00, 0x02, 0x00, 0x00, 0x03, 0x30, 0x00, 0x01, 0xe0, 0x80, 0x01,
0x80, 0xc3, 0x00, 0x00, 0xef, 0x00, 0xff, 0xff, 0xff, 0x03, 0x00, 0xc0, 0xf9, 0xff, 0xdf, 0x09, 0x00, 0xd0, 0x09, 0x00, 0xd0, 0x89, 0xc1,
0xd1, 0xe9, 0x81, 0xd3, 0x69, 0x00, 0xd6, 0x09, 0x91, 0xd0, 0x09, 0xdb, 0xd0, 0x09, 0x7e, 0xd0, 0x0d, 0x00, 0xd0, 0x4d, 0x89, 0xdb, 0xfb,
0xff, 0xdf, 0x03, 0x00, 0xc0, 0xff, 0xff, 0xff, 0x78, 0x00, 0x1e, 0x30, 0x00, 0x0c};
//128*32小电视bilibili                                                    
const  uint8_t bilibilitv_12832[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x1C,0x00,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x1C,0x00,0x0E,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x7C,0xC0,0x0F,0x00,0x1E,0x00,0x00,0x00,0xE0,0x03,0x00,0x00,0x00,0x00,
0x00,0x00,0xF0,0xE0,0x03,0x80,0x1F,0x00,0x00,0x00,0xF8,0x03,0x00,0x00,0x00,0x00,
0x00,0xF8,0xFF,0xFF,0xFF,0x83,0x1F,0x00,0x00,0x00,0xF8,0x03,0x00,0x00,0x00,0x00,
0x00,0xFC,0xFF,0xFF,0xFF,0x8F,0x1F,0x00,0x80,0x07,0xF8,0x03,0x00,0x78,0x00,0x00,
0x00,0xFE,0xFF,0xFF,0xFF,0x9F,0x1F,0x00,0x80,0x0F,0xF8,0x03,0x00,0xF8,0x00,0x00,
0x00,0xFE,0xFF,0xFF,0xFF,0x9F,0x1F,0x00,0x80,0x0F,0xF8,0x03,0x00,0xF8,0x00,0x00,
0x00,0x3F,0x00,0x00,0x80,0x9F,0x1F,0x00,0x80,0x0F,0xF0,0x03,0x00,0xF0,0x00,0x00,
0x00,0x1F,0x00,0x00,0x00,0xBE,0x1F,0x00,0x80,0x0F,0xF0,0x03,0x00,0xF0,0x00,0x00,
0x00,0x1F,0x00,0x00,0x00,0xBE,0x1F,0x00,0x00,0x0F,0xF0,0x03,0x00,0xF0,0x00,0x00,
0x00,0x1F,0x00,0x00,0x00,0xBE,0x1F,0x00,0x7C,0xEF,0xF3,0x03,0xC0,0xF7,0x7E,0x00,
0x00,0x1F,0x7F,0xC0,0x1F,0x3E,0x1F,0x00,0x7C,0xEF,0xF3,0x03,0xC0,0xF7,0x7E,0x00,
0x00,0xDF,0xFF,0xC0,0x3F,0x3E,0x1F,0x00,0x7C,0xEF,0xE3,0x03,0xC0,0xF7,0x78,0x00,
0x00,0x1F,0x1F,0x00,0x3E,0x3E,0x1F,0x00,0x18,0xEF,0xE3,0x03,0x80,0xE0,0x78,0x00,
0x00,0x1F,0x00,0x00,0x00,0x3E,0x1F,0x00,0x78,0x1F,0xE0,0x03,0x80,0xEF,0x00,0x00,
0x00,0x1F,0x00,0x00,0x00,0x3E,0x1E,0x00,0x78,0xFE,0xE3,0x03,0x80,0xEF,0x79,0x00,
0x00,0x1F,0xC0,0x4C,0x00,0x3E,0x3E,0x00,0xF8,0xFE,0xE3,0x03,0x80,0xEF,0x79,0x00,
0x00,0x1F,0xC0,0xFF,0x00,0x3E,0xFE,0x1F,0xF0,0xDE,0xE3,0xFF,0x87,0xEF,0xF9,0x00,
0x00,0x1F,0xC0,0xFF,0x00,0x3E,0xFE,0xFF,0xF0,0xDE,0xE3,0xFF,0x9F,0xEF,0xF9,0x00,
0x00,0x1F,0x80,0x3B,0x00,0x3E,0xFE,0xFF,0xF7,0xDE,0xE3,0xFF,0x7F,0xEF,0xF9,0x00,
0x00,0x1F,0x00,0x00,0x00,0x3E,0xFE,0xFF,0xFF,0xDE,0xE3,0xFF,0xFF,0xCF,0xF9,0x00,
0x00,0x3F,0x00,0x00,0x80,0x1F,0xFC,0xF8,0xEF,0xDF,0xC3,0x3F,0xFF,0xDF,0xF9,0x00,
0x00,0xFE,0xFF,0xFF,0xFF,0x1F,0xFC,0xFD,0xEF,0xDD,0xC3,0xBF,0xFF,0xDE,0xF1,0x00,
0x00,0xFE,0xFF,0xFF,0xFF,0x1F,0xFC,0xFF,0xEF,0xDD,0xC3,0xFF,0xFF,0xDE,0xF1,0x00,
0x00,0xFC,0xFF,0xFF,0xFF,0x0F,0xFC,0xFF,0xE3,0xDD,0xC3,0xFF,0x3F,0xDE,0xF1,0x00,
0x00,0xFC,0xFF,0xFF,0xFF,0x0F,0xFC,0xFF,0xE3,0xDD,0xC3,0xFF,0x3F,0xDE,0xF1,0x00,
0x00,0x00,0x0F,0x00,0x3E,0x00,0xFC,0x1F,0x00,0x00,0xC0,0xFF,0x01,0x00,0x00,0x00,
0x00,0x00,0x0F,0x00,0x3E,0x00,0xFC,0x1F,0x00,0x00,0xC0,0xFF,0x01,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};       

//下面是天气图标和对应心知天气的天气代码
//Sunny = 0 晴
const uint8_t Sunny[] U8X8_PROGMEM ={
0x00,0xC0,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0xC0,0x00,0x00,0x00,0xC0,0x00,0x00,
0x30,0xC0,0x00,0x03,0x70,0x00,0x80,0x03,0xE0,0x00,0xC0,0x01,0xC0,0xE0,0xC1,0x00,
0x00,0xF8,0x07,0x00,0x00,0xFE,0x1F,0x00,0x00,0xFE,0x1F,0x00,0x00,0xFF,0x3F,0x00,
0x00,0xFF,0x3F,0x00,0x80,0xFF,0x7F,0x00,0x8F,0xFF,0x7F,0x3C,0x8F,0xFF,0x7F,0x3C,
0x80,0xFF,0x7F,0x00,0x80,0xFF,0x7F,0x00,0x00,0xFF,0x3F,0x00,0x00,0xFE,0x1F,0x00,
0x00,0xFE,0x1F,0x00,0x00,0xFC,0x0F,0x00,0xC0,0xF0,0xC3,0x00,0xE0,0x00,0xC0,0x01,
0x70,0x00,0x80,0x03,0x30,0x00,0x00,0x03,0x00,0xC0,0x00,0x00,0x00,0xC0,0x00,0x00,
0x00,0xC0,0x00,0x00,0x00,0xC0,0x00,0x00                                            
};
//Cloudy = 4 多云
const uint8_t Cloudy[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0x00,
0x00,0x80,0x3F,0x00,0x00,0xE6,0xFF,0x00,0x00,0xFF,0xFF,0x00,0x80,0xFF,0xFF,0x00,
0x80,0xFF,0xFF,0x01,0xC0,0xFF,0xFF,0x01,0xC0,0xFF,0xFF,0x03,0xE0,0xFF,0xFF,0x07,
0xF0,0xFF,0xFF,0x0F,0xF8,0xFF,0x07,0x0F,0xF8,0xFF,0x71,0x0F,0xF8,0xFF,0xFE,0x0E,
0xF8,0x7F,0xFE,0x08,0xF0,0x7F,0xFF,0x07,0xE0,0x1F,0xFF,0x0F,0xC0,0xEF,0xFF,0x1F,
0x80,0xE7,0xFF,0x1F,0x00,0xF0,0xFF,0x3F,0x1C,0xF0,0xFF,0x3F,0x3C,0xF0,0xFF,0x1F,
0x7E,0xF0,0xFF,0x1F,0x7F,0xE0,0xFF,0x0F,0x3E,0xC0,0xFF,0x07,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00                                            
};
//Overcast = 9 阴
const uint8_t Overcast[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x80,0x1F,0x00,0x00,0xE0,0x7F,0x00,0x00,0xF3,0xFF,0x00,
0xC0,0xFF,0xFF,0x01,0xE0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x03,
0xF0,0xFF,0xFF,0x03,0xF8,0xFF,0xFF,0x07,0xFC,0xFF,0xFF,0x0F,0xFE,0xFF,0xFF,0x1F,
0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x3F,0xFF,0xFF,0xFF,0x3F,0xFF,0xFF,0xFF,0x3F,
0xFF,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x0F,0xF8,0xFF,0xFF,0x07,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00                                      
};
//Shower = 10 阵雨
const uint8_t Shower[] U8X8_PROGMEM ={
0x00,0x00,0x70,0x00,0x00,0x00,0xFC,0x03,0x00,0x00,0xFE,0x07,0x00,0x00,0xF8,0x0F,
0x00,0xC0,0xE3,0x1F,0x00,0xF0,0xCF,0x1F,0xC0,0xF9,0x9F,0x3F,0xE0,0xFF,0xBF,0x3F,
0xF0,0xFF,0x3F,0x3F,0xF0,0xFF,0x7F,0x1F,0xF0,0xFF,0x7F,0x1E,0xF8,0xFF,0xFF,0x1C,
0xFC,0xFF,0xFF,0x09,0xFE,0xFF,0xFF,0x03,0xFE,0xFF,0xFF,0x03,0xFF,0xFF,0xFF,0x07,
0xFF,0xFF,0xFF,0x07,0xFF,0xFF,0xFF,0x07,0xFF,0xFF,0xFF,0x03,0xFE,0xFF,0xFF,0x03,
0xFC,0xFF,0xFF,0x01,0xF0,0xFF,0x7F,0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x20,0x00,
0x60,0x60,0x30,0x00,0x60,0x60,0x30,0x00,0x30,0x30,0x18,0x00,0x30,0x30,0x18,0x00,
0x18,0x18,0x0C,0x00,0x08,0x08,0x04,0x00                                           
};
//Thundershower = 11 雷阵雨
const uint8_t Thundershower[] U8X8_PROGMEM ={
0x00,0x00,0x0E,0x00,0x00,0xC0,0x3F,0x00,0x00,0xE0,0xFF,0x00,0xC0,0xFF,0xFF,0x01,
0xE0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x03,0xF0,0xFF,0xFF,0x03,
0xF0,0xFF,0xFF,0x03,0xF8,0xFF,0xFF,0x07,0xFC,0xFF,0xFF,0x0F,0xFE,0xFF,0xFF,0x1F,
0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,
0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x07,0xF0,0xFF,0xFF,0x01,
0x00,0xF0,0x01,0x00,0x60,0xF0,0xC1,0x00,0x60,0xF0,0xC0,0x00,0x60,0x78,0xC0,0x00,
0x30,0xFC,0x60,0x00,0x30,0x70,0x60,0x00,0x10,0x30,0x20,0x00,0x10,0x18,0x20,0x00,
0x00,0x08,0x00,0x00,0x00,0x00,0x00,0x00                                           
};
//Rain_L = 13 小雨
const uint8_t Rain_L[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0x00,0x00,0xC0,0x3F,0x00,0x00,0xE0,0xFF,0x00,
0xC0,0xFF,0xFF,0x01,0xE0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x03,
0xF0,0xFF,0xFF,0x03,0xF0,0xFF,0xFF,0x03,0xF8,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x0F,
0xFE,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,
0xFF,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x07,
0xE0,0xFF,0xFF,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x60,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x30,0x00,0x00,0x00,0x10,0x00,0x00,
0x00,0x18,0x00,0x00,0x00,0x00,0x00,0x00                                            
};
//Rain_M = 14 中雨
const uint8_t Rain_M[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0x00,0x00,0xC0,0x3F,0x00,0x00,0xE0,0xFF,0x00,
0xC0,0xFF,0xFF,0x01,0xE0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x03,
0xF0,0xFF,0xFF,0x03,0xF0,0xFF,0xFF,0x03,0xF8,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x0F,
0xFE,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,
0xFF,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x07,
0xE0,0xFF,0xFF,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x0C,0x0C,0x00,0x00,0x04,0x04,0x00,0x00,0x06,0x06,0x00,0x00,0x02,0x02,0x00,
0x00,0x03,0x03,0x00,0x00,0x00,0x00,0x00                                   
};
//Rain_H = 15 大雨
const uint8_t Rain_H[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0x00,0x00,0xC0,0x3F,0x00,0x00,0xE0,0xFF,0x00,
0xC0,0xFF,0xFF,0x01,0xE0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x01,0xF0,0xFF,0xFF,0x03,
0xF0,0xFF,0xFF,0x03,0xF0,0xFF,0xFF,0x03,0xF8,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x0F,
0xFE,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,0xFF,0xFF,0xFF,0x1F,
0xFF,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x0F,0xFC,0xFF,0xFF,0x07,
0xE0,0xFF,0xFF,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x60,0x60,0x60,0x00,0x20,0x20,0x20,0x00,0x30,0x30,0x30,0x00,0x10,0x10,0x10,0x00,
0x18,0x18,0x18,0x00,0x00,0x00,0x00,0x00                                            
};
//Foggy = 30 雾
const uint8_t Foggy[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x1E,0x00,0x00,0xC0,0xFF,0x00,0x00,0xF0,0xFF,0x01,0x00,
0xF8,0xFF,0x03,0x00,0xFC,0xFF,0x07,0x00,0xFC,0xFF,0x0F,0x00,0xFE,0xFF,0xFF,0x01,
0xFE,0xFF,0xFF,0x07,0xFE,0xFF,0xFF,0x0F,0xFE,0xFF,0xFF,0x0F,0xFE,0xFF,0xFF,0x1F,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0xFF,0xFF,0x1F,
0xFE,0xFF,0xFF,0x1F,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0xFE,0xFF,0xFF,0x1F,0xFE,0xFF,0xFF,0x1F,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0xFF,0xFF,0x1F,
0xFE,0xFF,0xFF,0x1F,0x00,0x00,0x00,0x00                                            
};
//Haze = 31 霾
const uint8_t Haze[] U8X8_PROGMEM ={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0xE0,0x00,0x1C,0x0E,0xE0,0x00,0x1C,
0x0E,0xE0,0x00,0x1C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0xF0,0x03,0xF0,0x01,0xF8,0x0F,0xFC,0x07,0x1C,0x1E,0x0E,0x0E,
0x0E,0x38,0x07,0x1C,0x06,0x30,0x03,0x18,0x07,0xF0,0x03,0x38,0x03,0xE0,0x01,0x30,
0x03,0xE0,0x01,0x30,0x07,0x30,0x03,0x38,0x06,0x38,0x07,0x18,0x0E,0x1C,0x0E,0x1C,
0x1C,0x0F,0x1C,0x0F,0xF8,0x07,0xF8,0x03,0xF0,0x03,0xF0,0x01,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0E,0xE0,0x00,0x1C,0x0E,0xE0,0x00,0x1C,
0x0E,0xE0,0x00,0x1C,0x00,0x00,0x00,0x00                                            
};

#endif

