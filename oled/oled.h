#ifndef _oled_h
#define _oled_h

#include "main.h"

void oled_init(void);
int oled_sample(void);

#endif

