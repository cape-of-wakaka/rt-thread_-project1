#include "rgb_led.h"
#include "main.h"


void rgb_led_init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, RGB3_Pin|RGB2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED1_Pin|LED5_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RGB1_Pin|RGB6_Pin|RGB5_Pin|RGB4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED6_Pin|LED4_Pin|LED3_Pin|LED2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : PAPin PAPin PAPin PAPin */
  GPIO_InitStruct.Pin = RGB3_Pin|RGB2_Pin|LED1_Pin|LED5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PBPin PBPin PBPin PBPin
                           PBPin PBPin PBPin PBPin */
  GPIO_InitStruct.Pin = RGB1_Pin|RGB6_Pin|LED6_Pin|LED4_Pin
                          |RGB5_Pin|RGB4_Pin|LED3_Pin|LED2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}


void rgb_write1(uint8_t param)
{
  switch(param)
	{
	  case 1:
			RGB1_H;
			break;
		case 2:
			RGB2_H;
			break;
		case 3:
			RGB3_H;
			break;
		case 4:
			RGB4_H;
			break;
		case 5:
			RGB5_H;
			break;
		case 6:
			RGB6_H;
			break;
		default: break;
	}
  __nop();__nop();__nop();__nop();//由于不同编译器的编译效率的不同，需要根据逻辑分析仪进行波形分析
	
  switch(param)
	{
	  case 1:
			RGB1_L;
			break;
		case 2:
			RGB2_L;
			break;
		case 3:
			RGB3_L;
			break;
		case 4:
			RGB4_L;
			break;
		case 5:
			RGB5_L;
			break;
		case 6:
			RGB6_L;
			break;
		default: break;
	}
	__nop();__nop();//由于不同编译器的编译效率的不同，需要根据逻辑分析仪进行波形分析
}

void rgb_write0(uint8_t param)
{
  switch(param)
	{
	  case 1:
			RGB1_H;
			break;
		case 2:
			RGB2_H;
			break;
		case 3:
			RGB3_H;
			break;
		case 4:
			RGB4_H;
			break;
		case 5:
			RGB5_H;
			break;
		case 6:
			RGB6_H;
			break;
		default: break;
	}
  __nop();__nop();//由于不同编译器的编译效率的不同，需要根据逻辑分析仪进行波形分析
	
  switch(param)
	{
	  case 1:
			RGB1_L;
			break;
		case 2:
			RGB2_L;
			break;
		case 3:
			RGB3_L;
			break;
		case 4:
			RGB4_L;
			break;
		case 5:
			RGB5_L;
			break;
		case 6:
			RGB6_L;
			break;
		default: break;
	}
	__nop();__nop();__nop();__nop();//由于不同编译器的编译效率的不同，需要根据逻辑分析仪进行波形分析
}



void rgb_reset(uint8_t param)
{
	switch(param)
	{
	  case 1:
			RGB1_L;
			break;
		case 2:
			RGB2_L;
			break;
		case 3:
			RGB3_L;
			break;
		case 4:
			RGB4_L;
			break;
		case 5:
			RGB5_L;
			break;
		case 6:
			RGB6_L;
			break;
		default: break;
	}
	delay_us(80);
}

void rgb_write_byte(uint8_t param,uint8_t byte)
{
	uint8_t i;


	for(i=0;i<8;i++)
		{
			
			
			
			if(byte&0x80)
				{
					rgb_write1(param);
			}
			else
				{
					rgb_write0(param);
			}
		byte <<= 1;
	}
}




void rgb_write_24bits(uint8_t param,uint8_t green,uint8_t red,uint8_t blue)
{
	rgb_write_byte(param,green);
	rgb_write_byte(param,red);
	rgb_write_byte(param,blue);
}


//亮灯颜色设定，其他颜色以此类推
void rgb_red(uint8_t param)
{
	 uint8_t i;
	//5个LED全彩灯
	for(i=0;i<5;i++)
		{
			rgb_write_24bits(param,0, 0xff, 0);
	}
}

void rgb_green(uint8_t param)
{
	uint8_t i;

	for(i=0;i<5;i++)
		{
			rgb_write_24bits(param,0xff, 0, 0);
	}
}

void rgb_blue(uint8_t param)
{
	uint8_t i;

	for(i=0;i<5;i++)
		{
			rgb_write_24bits(param,0, 0, 0xff);
	}
}














