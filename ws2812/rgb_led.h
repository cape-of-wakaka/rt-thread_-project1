#ifndef _rgb_led_h
#define _rgb_led_h

#include "main.h"

#define RGB1_Pin GPIO_PIN_0
#define RGB1_GPIO_Port GPIOB

#define RGB2_Pin GPIO_PIN_2
#define RGB2_GPIO_Port GPIOA

#define RGB3_Pin GPIO_PIN_1
#define RGB3_GPIO_Port GPIOA

#define RGB4_Pin GPIO_PIN_5
#define RGB4_GPIO_Port GPIOB

#define RGB5_Pin GPIO_PIN_4
#define RGB5_GPIO_Port GPIOB

#define RGB6_Pin GPIO_PIN_1
#define RGB6_GPIO_Port GPIOB

#define LED1_Pin GPIO_PIN_3
#define LED1_GPIO_Port GPIOA

#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOB

#define LED3_Pin GPIO_PIN_6
#define LED3_GPIO_Port GPIOB

#define LED4_Pin GPIO_PIN_3
#define LED4_GPIO_Port GPIOB

#define LED5_Pin GPIO_PIN_15
#define LED5_GPIO_Port GPIOA

#define LED6_Pin GPIO_PIN_10
#define LED6_GPIO_Port GPIOB


#define RGB1_H    HAL_GPIO_WritePin(RGB1_GPIO_Port,RGB1_Pin,GPIO_PIN_SET)
#define RGB1_L    HAL_GPIO_WritePin(RGB1_GPIO_Port,RGB1_Pin,GPIO_PIN_RESET)

#define RGB2_H    HAL_GPIO_WritePin(RGB2_GPIO_Port,RGB2_Pin,GPIO_PIN_SET)
#define RGB2_L    HAL_GPIO_WritePin(RGB2_GPIO_Port,RGB2_Pin,GPIO_PIN_RESET)

#define RGB3_H    HAL_GPIO_WritePin(RGB3_GPIO_Port,RGB3_Pin,GPIO_PIN_SET)
#define RGB3_L    HAL_GPIO_WritePin(RGB3_GPIO_Port,RGB3_Pin,GPIO_PIN_RESET)

#define RGB4_H    HAL_GPIO_WritePin(RGB4_GPIO_Port,RGB4_Pin,GPIO_PIN_SET)
#define RGB4_L    HAL_GPIO_WritePin(RGB4_GPIO_Port,RGB4_Pin,GPIO_PIN_RESET)

#define RGB5_H    HAL_GPIO_WritePin(RGB5_GPIO_Port,RGB5_Pin,GPIO_PIN_SET)
#define RGB5_L    HAL_GPIO_WritePin(RGB5_GPIO_Port,RGB5_Pin,GPIO_PIN_RESET)

#define RGB6_H    HAL_GPIO_WritePin(RGB6_GPIO_Port,RGB6_Pin,GPIO_PIN_SET)
#define RGB6_L    HAL_GPIO_WritePin(RGB6_GPIO_Port,RGB6_Pin,GPIO_PIN_RESET)



#define LED1_H    HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_SET)
#define LED1_L    HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_RESET)

#define LED2_H    HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_SET)
#define LED2_L    HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_RESET)

#define LED3_H    HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,GPIO_PIN_SET)
#define LED3_L    HAL_GPIO_WritePin(LED3_GPIO_Port,LED3_Pin,GPIO_PIN_RESET)

#define LED4_H    HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,GPIO_PIN_SET)
#define LED4_L    HAL_GPIO_WritePin(LED4_GPIO_Port,LED4_Pin,GPIO_PIN_RESET)

#define LED5_H    HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,GPIO_PIN_SET)
#define LED5_L    HAL_GPIO_WritePin(LED5_GPIO_Port,LED5_Pin,GPIO_PIN_RESET)

#define LED6_H    HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,GPIO_PIN_SET)
#define LED6_L    HAL_GPIO_WritePin(LED6_GPIO_Port,LED6_Pin,GPIO_PIN_RESET)


void rgb_led_init(void);
void rgb_write0(uint8_t param);
void rgb_write0(uint8_t param);


#endif

