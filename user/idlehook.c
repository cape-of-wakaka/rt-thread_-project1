#include "main.h"


//空闲任务钩子函数
//创建一个线程，通过延时进入空闲任务钩子，打印进入空闲任务钩子的次数

#define THREAD_PRIORITY 20
#define THREAD_STACK_SIZE 1024
#define THREAD_TIMESLICE 5

//指向指针控制块的指针
static rt_thread_t tid=RT_NULL;

//空闲钩子函数执行次数
volatile static int hook_times=0;

//空闲任务钩子函数
static void idle_hook()
{
 if(0==(hook_times%10000))
 {
	 rt_kprintf("enter idle hook %d times.\n",hook_times);
 }
  rt_enter_critical();
  hook_times++;
  rt_exit_critical();

}

//线程入口函数
static void thread_entry(void *param)
{
 int i=5;
	while(i--)
	{
	 rt_kprintf("enter thread1.\n");
	 rt_enter_critical();
	 hook_times=0;
	 rt_exit_critical();
		
	 //休眠500ms
		rt_kprintf("thread1 delay 500 OS tick\n");
		rt_thread_mdelay(500);
	}
	rt_kprintf("delate idle hook\n");
	
	//删除空闲钩子函数
	rt_thread_idle_delhook(idle_hook);
	rt_kprintf("thread1 finish\n");

}

int idle_hook_sample()
{
 //设置空闲钩子函数
	rt_thread_idle_sethook(idle_hook);
 //创建线程
	tid=rt_thread_create("thread1",
	                     thread_entry,RT_NULL,
	                     THREAD_STACK_SIZE,
	                     THREAD_PRIORITY,THREAD_TIMESLICE
	                     );
	if(tid!=RT_NULL)
		rt_thread_startup(tid);


 return 0;
}

MSH_CMD_EXPORT(idle_hook_sample,idle hook sample);























