#include "key_scan.h"

#include "main.h"


#define THREAD_PRIORITY 20
#define THREAD_STACK_SIZE 128
#define THREAD_TIMESLICE 10

//邮箱控制块
rt_mailbox_t mailbox1=RT_NULL;

//用于存放邮件的内存池
char mb_pool[12];

int mailbox_sample(void)
{
  
	//初始化一个mailbox
	mailbox1=rt_mb_create("mbt1",
										sizeof(mb_pool)/4,//邮箱邮件数目
										RT_IPC_FLAG_FIFO//采用FIFO方式进行线程等待
										);

	  if (mailbox1 != RT_NULL)
    {
        rt_kprintf("init mailbox success.\n");
        return -1;
    }
		
		return 0;
}

/****************************线程入口函数*****************************************/


void key_init(void)
{	
	
  MX_KEY_Init();	
}

void key_entry(void *param)
{
		//变量需定义在main线程中
	
  uint8_t key=0;
	 while(1)
	 {
		key=key_scan();
		if(key)
		{
			switch(key)
			{
				case(KEY1_Press):
					
					break;
				case(KEY2_Press):
					
					break;		
				case(KEY3_Press):
					
					break;
        default:break;				
			}
			rt_mb_send(mailbox1,key);//发送按键值到邮箱
		}
    rt_thread_mdelay(200);
	 }
}
/*****************************用户应用程序***************************/
int key_sample(void)
{
	mailbox_sample();
	key_init();
	//指向指针控制块的指针
    rt_thread_t thread;
	
	//创建名为thread2的线程									
	thread=rt_thread_create("thread_key",
	                        key_entry,
	                        RT_NULL,
							            THREAD_STACK_SIZE,
							            THREAD_PRIORITY,
							            THREAD_TIMESLICE											 
	                       );
	if(thread!=RT_NULL)
		rt_thread_startup(thread);//将线程的状态更改为就绪状态，并放到相应优先级队列中等待调度
  
	
	
  return 0;	

}

MSH_CMD_EXPORT(key_sample,key sample);


/********************************************************************************************************************/
/*
线程睡眠：需要让运行的当前线程延迟一段时间，在指定的
时间到达后重新运行，叫做“线程睡眠”
线程睡眠使用以下三个函数：

rt_err_t rt_thread_sleep(rt_tick_t tick);
rt_err_t rt_thread_delay(rt_tick_t tick);
rt_err_t rt_thread_mdelay(rt_int32_t ms);

这三个函数接口的作用相同，调用它们可以使当前线程挂起一段指定的时间，
当这个时间过后，线程会被唤醒并再次进入就绪状态

*******************************************************************/


/********************************************************
rt_thread_create()创建动态线程
rt_thread_init()初始化一个静态线程

1.动态线程是系统自动从动态内存堆上分配栈空间与线程句柄
（初始化heap之后才能使用create创建动态线程）；静态线程
是由用户分配栈空间与线程句柄（内核创建线程）
分配的栈空间是按照rtconfig.h中配置的RT_ALIGN_SIZE方式对齐
2.脱离线程列表方式
  rt_thread_create() ---> rt_thread_delete()
  rt_thread_init() ----> rt_thread_detach()
********************************************************/


/*******************************************************
RO Size Flash
code:代码段，存放程序的代码部分
RO-data:只读数据段，存放程序中定义的常量const

RW Size RAM
RW-data:读写数据段，存放初始化为非0的全局变量
ZI-data:0数据段，存放未初始化的全局变量及初始化为0的变量

*********************************************************/


















