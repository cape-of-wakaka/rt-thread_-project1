#ifndef _delay_h
#define _delay_h

#include "main.h"

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);

#endif
