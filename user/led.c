#include "main.h"
#include "led.h"

void led_init(void)
{		
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	  /*Configure GPIO pin : PtPin */
  //GPIO_InitStruct.Pin = GPIO_PIN_13;
	GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  //HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}


static rt_thread_t led1_thread=RT_NULL;
static void led1_thread_entry(void* paramter);


	
	
	
	
static void led1_thread_entry(void* parameter)
{
  while(1)
	{
		
	 HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);
	 rt_thread_mdelay(1000);
	 //rt_kprintf("led1_thread running,LED1_ON\r\n");
		
	 HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);	
	 rt_thread_mdelay(1000);
	  //rt_kprintf("led1_thread running,LED1_OFF\r\n");
	
	}
}
int led_sample()
{
  led_init();
  led1_thread=               //线程控制块指针
	rt_thread_create("led1",     //线程名称
						 led1_thread_entry,//线程入口函数
						 RT_NULL,          //线程入口函数参数
						 512,              //线程栈大小
						 3,                //线程的优先级
						 20);              //线程时间片
//启动线程，开始调度
	if(led1_thread!=RT_NULL)
		rt_thread_startup(led1_thread);
	else
		return -1;
 return 0;
}

MSH_CMD_EXPORT(led_sample, led sample);
