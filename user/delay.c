#include "delay.h"


extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim10;


void delay_us(uint32_t us)
{
    uint16_t counter=us&0xffff;

    __HAL_TIM_SetCounter(&htim1,counter);
	  //TIM1->ARR=counter;
    HAL_TIM_Base_Start(&htim1);
	
    while(counter>=1)
    {
        counter=__HAL_TIM_GetCounter(&htim1);
    }

    HAL_TIM_Base_Stop(&htim1);
}

void delay_ms(uint32_t ms)
{
	 uint16_t i=0;
	 for(i=0;i<ms;i++)
   delay_us(1000);
	 
}
