#include "main.h"
#include "adc_convertor.h"

#define THREAD_PRIORITY 20
#define THREAD_STACK_SIZE 4096
#define THREAD_TIMESLICE 10

//#define  debug

extern UART_HandleTypeDef huart6;
extern ADC_HandleTypeDef hadc1;

__IO uint16_t ADC_ConvertedValue[1024];

extern rt_mailbox_t mailbox1;
/****************************线程入口函数*****************************************/

//创建event控制块
rt_event_t event;

void event_init()
{
  event=rt_event_create("event", RT_IPC_FLAG_FIFO);
	if(event!=RT_NULL)
	{
	 rt_kprintf("事件创建成功\n");
	}
}

void adc_init(void)
{		
  MX_ADC1_Init();
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)ADC_ConvertedValue, 1024);
	
}

extern uint8_t serial_data[16];

void adc_entry(void *param)
{
	//变量需定义在main线程中
	adc_init();
	event_init();//初始化事件
	
  int i = 0, ADC_Value = 0;
	uint8_t key_value=0;
	uint8_t serial_data1[16]={0};
	 while(1)
	 {
		 #ifdef debug
	   	for(i = 0; i < 20; ++i)
			{
			 ADC_Value += ADC_ConvertedValue[i+1000];
			}
			ADC_Value = ADC_Value / 20;
			rt_kprintf("ADC:%d\n",ADC_Value);
			HAL_UART_Transmit(&huart6, serial_data1,16,1000);
			ADC_Value=0;
		#endif
		  delay_ms(100); 
			rt_thread_mdelay(100);
		 //按键邮箱信息
			if(rt_mb_recv(mailbox1,(rt_uint32_t *)&key_value,10)==RT_EOK)
			rt_kprintf("KEY_VALUE:%d\n",key_value);
			
	 }
}
/*****************************用户应用程序***************************/
int adc_sample(void)
{
	
	//指向指针控制块的指针
    rt_thread_t thread;
	
	//创建名为thread2的线程									
	thread=rt_thread_create("thread_adc",
	                        adc_entry,
	                        RT_NULL,
							            THREAD_STACK_SIZE,
							            THREAD_PRIORITY,
							            THREAD_TIMESLICE											 
	                       );
	if(thread!=RT_NULL)
		rt_thread_startup(thread);//将线程的状态更改为就绪状态，并放到相应优先级队列中等待调度
  
	
	
  return 0;	

}

MSH_CMD_EXPORT(adc_sample,adc sample);


/********************************************************************************************************************/
/*
线程睡眠：需要让运行的当前线程延迟一段时间，在指定的
时间到达后重新运行，叫做“线程睡眠”
线程睡眠使用以下三个函数：

rt_err_t rt_thread_sleep(rt_tick_t tick);
rt_err_t rt_thread_delay(rt_tick_t tick);
rt_err_t rt_thread_mdelay(rt_int32_t ms);

这三个函数接口的作用相同，调用它们可以使当前线程挂起一段指定的时间，
当这个时间过后，线程会被唤醒并再次进入就绪状态

*******************************************************************/


/********************************************************
rt_thread_create()创建动态线程
rt_thread_init()初始化一个静态线程

1.动态线程是系统自动从动态内存堆上分配栈空间与线程句柄
（初始化heap之后才能使用create创建动态线程）；静态线程
是由用户分配栈空间与线程句柄（内核创建线程）
分配的栈空间是按照rtconfig.h中配置的RT_ALIGN_SIZE方式对齐
2.脱离线程列表方式
  rt_thread_create() ---> rt_thread_delete()
  rt_thread_init() ----> rt_thread_detach()
********************************************************/


/*******************************************************
RO Size Flash
code:代码段，存放程序的代码部分
RO-data:只读数据段，存放程序中定义的常量const

RW Size RAM
RW-data:读写数据段，存放初始化为非0的全局变量
ZI-data:0数据段，存放未初始化的全局变量及初始化为0的变量

*********************************************************/


















