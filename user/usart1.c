#include "usart1.h"
#include <rtthread.h>


static UART_HandleTypeDef  UART1_Hander;
static GPIO_InitTypeDef GPIO_InitStruct = {0};

void usart1_init()
{
  //UART_HandleTypeDef UART1_Hander;
	
	UART1_Hander.Instance=USART1;
	UART1_Hander.Init.BaudRate=115200;
	UART1_Hander.Init.HwFlowCtl=UART_HWCONTROL_NONE;
	UART1_Hander.Init.Mode=UART_MODE_TX_RX;
	UART1_Hander.Init.OverSampling=UART_OVERSAMPLING_16;
	UART1_Hander.Init.WordLength=UART_WORDLENGTH_8B;
	UART1_Hander.Init.StopBits=UART_STOPBITS_1;
	UART1_Hander.Init.Parity=UART_PARITY_NONE;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();			//使能GPIOA时钟
	__HAL_RCC_USART1_CLK_ENABLE();			//使能USART1时钟
	
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pins : PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	
	if(HAL_UART_Init(&UART1_Hander)!=HAL_OK)
	{
		while(1);
	}
 
}
//INIT_BOARD_EXPORT(usart1_init);


//void rt_hw_console_output(const char *str)
//{

//    rt_size_t i = 0, size = 0;
//    char a = '\r';

//    __HAL_UNLOCK(&UART1_Hander);

//    size = rt_strlen(str);
//    for (i = 0; i < size; i++)
//    {
//        if (*(str + i) == '\n')
//        {
//            HAL_UART_Transmit(&UART1_Hander, (uint8_t *)&a, 1, 1);
//        }
//        HAL_UART_Transmit(&UART1_Hander, (uint8_t *)(str + i), 1, 1);
//    }
//}


//char rt_hw_console_getchar(void)
//{
//    int ch = -1;

//    if (__HAL_UART_GET_FLAG(&UART1_Hander, UART_FLAG_RXNE) != RESET)
//    {
//        ch = UART1_Hander.Instance->DR & 0xff;
//    }
//    else
//    {
//        if(__HAL_UART_GET_FLAG(&UART1_Hander, UART_FLAG_ORE) != RESET)
//        {
//            __HAL_UART_CLEAR_OREFLAG(&UART1_Hander);
//        }
//        rt_thread_mdelay(10);
//    }
//    return ch;
//}
