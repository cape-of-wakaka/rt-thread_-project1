#include "sys_init.h"
#include "usart6.h"


extern int8_t serial_data[16];
extern SPI_HandleTypeDef hspi1;

void sys_init()
{

  MX_USART6_UART_Init();
	rgb_led_init();
	MX_TIM1_Init();
	MX_TIM3_Init();
	HAL_TIM_Base_Start_IT(&htim3);
	MX_SPI1_Init();
//	MX_TIM10_Init();
//	MX_TIM9_Init();
//	HAL_TIM_Base_Start_IT(&htim9);
	 HAL_UART_Receive_IT(&huart6,(uint8_t *)serial_data,16);
	 HAL_SPI_Receive_IT(&hspi1,(uint8_t *)serial_data,16);
	 
 

}







