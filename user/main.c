/*
  Based on STM32Cube_FW_F4_V1.25.0 library and rt-thread-v3.1.3
	Email:1216579118@qq.com 
	Author: ykk
*/
#include "main.h"
#include "rtthread.h"


int main(void)
{	
	 sys_init();  //系统外设初始化
	 adc_sample();//麦克分AD DMA数据转换线程
   key_sample();//按键数据读取线程
   oled_sample();//OLED U8G2显示线程
	 fft_sample(); //快速傅里叶变换线程1024点
	
	while(1)
  {
	
		HAL_GPIO_TogglePin(LED6_GPIO_Port,LED6_Pin);
		rt_thread_mdelay(1000);
		
	}	

}



